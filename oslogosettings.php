<?php
/**
 * @author Voloshchuk Yuri aka Vir Dignus
 * @copyright 2013©Voloshchuk Yuri
 * @version 0.0.1
 */
$ctx = $resource->context_key;
$templIDs = explode(',', $modx->getOption("templIDs", $scriptProperties, 'all')); //included templates, 'all' mean all templates.
$parentIDs = explode(',', $modx->getOption('parentIDS', $scriptProperties, '0')); //included parentns ids, set parents, for whom will be generate
if ($templIDs[0] == "all") {
    $in_template = true;
} else {
    $in_template = in_array($resource->template, $templIDs);
}
$in_parent = false;
//list of all parent ids
$p_list = $modx->getParentIds($id, 10, array('context' => $ctx));
foreach ($p_list as $p) {
    if (in_array($p, $parentIDs)) {
        $in_parent = true;
        break;
    }
}
$status = '';
if ($in_parent && $in_template) {
    /**
     * option block
     */
    $img_path = $modx->getOption('img_path', $scriptProperties, MODX_ASSETS_URL ."images/");
    $img_tv = $modx->getOption('tv_id', $scriptProperties, 1); // id of TV for image
    $def_img = $modx->getOption('def_img', $scriptProperties, 'assets/default.gif'); //default image name
    $imgHeight = $modx->getOption('imgHeight', $scriptProperties, 60); //heigth of cropped image
    $imgWidht = $modx->getOption('imgWidht', $scriptProperties, 60); //widht of cropped image
    $img = $resource->GetTVValue($img_tv); //get TV value
    $img_p = $resource->getProperty('oslogo','vacuum', ''); //get value from proprties field
    // $def_img = $def_img; //set default image
    $base_path = '';
    $status = "";
    if ($ctx == "web") {
        $base_path = $modx->getOption('base_path');
    } else {
        $modx->switchContext($ctx);
        $base_path = dirname(MODX_BASE_PATH) . '/' . $modx->getOption('http_host') . '/';
    }
        
    if ($img != $img_p) {
        $status = 'changed';
    }
    if ($img == "") {
        $status = 'empty';
        
    }

    if ($status == "") {
        $status = "ok";
    }
    switch ($status) {
        case 'empty':
            $img = $def_img;
            break;
        case 'changed':
            $img =  $img;
            break;

    }
    
    
    if ($status != "ok" and is_readable($base_path .$img)) {
        //check folder for thumbs
        if (!(file_exists($base_path .dirname($img) . '/thumbs/'))) {
            mkdir($base_path .dirname($img) . '/thumbs/', 0755);
        }
      $img2='';
        try {
             
            $img2=$img;
            $image = new Imagick($base_path .$img);
            
            $image->cropThumbnailImage($imgWidht, $imgHeight);
             
            // $image->setImageFormat('png');
            $thumb =$base_path . dirname($img) . '/thumbs/' . $imgWidht . 'x' . $imgHeight . '_' .
                basename($img);
               
            $image->writeImage($thumb);
            $img = dirname($img) . '/thumbs/' . $imgWidht . 'x' . $imgHeight . '_' .
                basename($img);
            if ($secondIimgWidht || $secondImgHeight) {
                $image->cropThumbnailImage($secondIimgWidht, $secondImgHeight);
                $s_thumb = $base_path .dirname($img2) . '/thumbs/' . $secondIimgWidht . 'x' . $secondImgHeight .
                    '_' . basename($img2);
                $image->writeImage($s_thumb);
                $s_img = dirname($img2) . '/thumbs/' . $secondIimgWidht . 'x' . $secondImgHeight .
                    '_' . basename($img2);

            }
            // Destroys Imagick object, freeing allocated resources in the process

            $image->destroy();
        }

        catch (ImagickException $e) {
            $modx->log(1, $e->getMessage());
            $image->destroy();
            $img = $def_img;
            $s_img = $def_img2;
        }

    } /*else {
        $img = $def_img;
        $s_img = $def_img2;
    }*/
    $resource->setTVValue($img_tv, $img);
    $resource->setProperties(array('introimage' => $img, 'sliderimage' => $s_img),'introtext');
    if ($introtext == "") {
        $resource->set('introtext', mb_substr(strip_tags($text, $allowTags), 0, $limit,
            'UTF-8'));
       
    } $resource->save();


}


?>