<?php
/**
 *
 *
 */
$doc = $modx->getObject('modResource',array('id'=>$hook->getValue('resource_id')));
if (empty($doc)){
    $doc = $modx->newObject('modResource');
    $doc->set('createdby', $modx->user->get('id'));
}
else{
    $doc->set('editedby', $modx->user->get('id'));
}
$allFormFields = $hook->getValues();
foreach ($allFormFields as $field=>$value)
{
    if ($field !== 'spam' && $field !== 'resource_id'){
        $doc->set($field, $value);
    }
}
$alias = $doc->cleanAlias($fields['pagetitle']);
if($modx->getCount(modResource, array('alias'=>$alias))!= 0) {
    $count = 1;
    $newAlias = $alias;
    while($modx->getCount(modResource, array('alias'=>$newAlias))!= 0) {
        $newAlias = $alias;
        $newAlias .= '-' . $count;
        $count++;
    }
    $alias = $newAlias;
}
$doc->set('alias',$alias);
$doc->set('template', $template);
$doc->save();
foreach ($allFormFields as $field=>$value)
{
    if (!empty($value) && $tv = $modx->getObject('modTemplateVar', array ('name'=>$field)))
    {
        /* handles checkboxes & multiple selects elements */
        if (is_array($value)) {
            $featureInsert = array();
            while (list($featureValue, $featureItem) = each($value)) {
                $featureInsert[count($featureInsert)] = $featureItem;
            }
            $value = implode('||',$featureInsert);
        }
        $tv->setValue($doc->get('id'), $value);
        $tv->save();
    }
}
$modx->cacheManager->refresh();
return true;